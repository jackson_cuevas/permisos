import { Routes } from '@angular/router';
import { CreatePermisosFormComponent } from './create-permisos-form/create-permisos-form.component';
import { HomeComponent } from './home/home.component';
import { PermisosListaComponent } from './permisos-lista/permisos-lista.component';


export const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'create-permisos-form', component: CreatePermisosFormComponent },
    { path: 'permisos-list', component: PermisosListaComponent },
    { path: '**', redirectTo: '', pathMatch: 'full' },
];
