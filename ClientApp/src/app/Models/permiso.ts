import { TipoPermiso } from './tipo-permiso';

export interface Permiso {
    id: number;
    employeeName: string;
    employeeLastName: string;
    tipoPermisoId: number;
    tipoPermiso: number;
    fechaPermiso: string;
}
