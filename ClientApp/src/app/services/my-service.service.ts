import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Permiso } from '../Models/Permiso';
import { TipoPermiso } from '../Models/tipo-permiso';
@Injectable({
    providedIn: 'root'
})
export class MyServiceService {

    baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

    getPermisos(): Observable<Permiso[]> {
        return this.http.get<Permiso[]>(`${this.baseUrl}/permisos`);
    }

    getTipoPermisos(): Observable<TipoPermiso[]> {
        return this.http.get<TipoPermiso[]>(`${this.baseUrl}/permisos/tipoPermisos`);
    }

    saveTipoPermisos(permiso): Observable<Permiso> {
        return this.http.post<Permiso>(`${this.baseUrl}/permisos`, permiso);
    }


    deleteTipoPermisos(id: number) {
        return this.http.delete<number>(`${this.baseUrl}/permisos/${id}`);
    }
}
