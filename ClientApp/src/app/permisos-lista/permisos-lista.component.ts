import { Component, OnInit } from '@angular/core';
import { MyServiceService } from '../services/my-service.service';
import { Permiso } from '../Models/Permiso';
import { AlertifyService } from '../services/alertify.service';

@Component({
  selector: 'app-permisos-lista',
  templateUrl: './permisos-lista.component.html',
  styleUrls: ['./permisos-lista.component.css']
})
export class PermisosListaComponent implements OnInit {
  permisos: Permiso[] = [];
  constructor(private myServiceService: MyServiceService, private alertifyService: AlertifyService) { }

  ngOnInit() {
    this.loadPermisos();
  }

  loadPermisos() {
    this.myServiceService.getPermisos().subscribe((permisos: Array<Permiso>) => {
      this.permisos = [...permisos];
    }, (error) => {
      console.log(error);
    });
  }

  borrar(id: number) {
    this.myServiceService.deleteTipoPermisos(id).subscribe(() => {
      this.alertifyService.success('Permiso eliminado con exito!');
      this.loadPermisos();
    }, (error) => {
        this.alertifyService.error('Hubo un error al eliminar el permiso');
    });
  }
}
