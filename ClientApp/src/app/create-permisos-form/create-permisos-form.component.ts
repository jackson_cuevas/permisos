import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TipoPermiso } from '../Models/tipo-permiso';
import { MyServiceService } from '../services/my-service.service';
import { Permiso } from '../Models/Permiso';
import { AlertifyService } from '../services/alertify.service';


@Component({
  selector: 'app-create-permisos-form',
  templateUrl: './create-permisos-form.component.html',
  styleUrls: ['./create-permisos-form.component.css']
})
export class CreatePermisosFormComponent implements OnInit {
  permisoForm: FormGroup;
  tipoPermisos: TipoPermiso[] = [];
  permisos: Permiso[] = [];
  submitted = false;
  constructor(private fb: FormBuilder, private myServiceService: MyServiceService, private alertifyService: AlertifyService) { }

  ngOnInit() {
    this.buildForm();
    this.loadTipoPermisos();
  }


 private buildForm() {
    this.permisoForm = this.fb.group({
      id: [''],
      employeeName: ['', Validators.required],
      employeeLastName: ['', Validators.required],
      tipoPermisoId: ['null', Validators.required],
      fechaPermiso: ['']
    });
  }

  loadTipoPermisos() {
    this.myServiceService.getTipoPermisos().subscribe((tipoPermiso: Array<TipoPermiso>) => {
      this.tipoPermisos = [...tipoPermiso];
    }, (error) => {
      console.log(error);
    });
  }


  
  onSubmit() {
    this.submitted = true;
    const userInformation = this.permisoForm.getRawValue();

    const request = {
      employeeName: userInformation.employeeName,
      employeeLastName: userInformation.employeeLastName,
      tipoPermisoId: userInformation.tipoPermisoId
    };

    this.myServiceService.saveTipoPermisos(request).subscribe((result) => {
      this.alertifyService.success('Permiso guardado con exito!');
      this.permisoForm.reset();
    }, (error) => {
      this.alertifyService.error('Hubo un error al guardar el permiso');
    });
  }

  cancel() {
    this.permisoForm.reset();
  }
}
